/*
 * Copyright 2020 Nuno A. C. Henriques https://nunoachenriques.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'package:s_alpha/pira.dart' as pira;
import 'package:url_launcher/url_launcher.dart';

void main() => runApp(SAlpha());

class SAlpha extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final String appTitle = "s α";

    return MaterialApp(
      title: appTitle,
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: SAlphaMain(title: appTitle),
    );
  }
}

class SAlphaMain extends StatefulWidget {
  final String title;

  SAlphaMain({Key key, this.title}) : super(key: key);

  @override
  _SAlphaMainState createState() => _SAlphaMainState(title);
}

class _SAlphaMainState extends State<SAlphaMain> {
  final String title;
  Future<List<pira.PIRA>> _regionsPIRA;

  _SAlphaMainState(this.title);

  @override
  void initState() {
    super.initState();
    _refreshPIRA();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Container(
          alignment: Alignment.center,
          padding: const EdgeInsets.all(0.0),
          child: new RefreshIndicator(
            onRefresh: _refreshPIRA,
            child: FutureBuilder<List<pira.PIRA>>(
              future: _regionsPIRA,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasData) {
                    return piraWidget(snapshot.data);
                  }
                } else if (snapshot.hasError) {
                  return Text("$snapshot.error");
                }
                return CircularProgressIndicator();
              },
            ),
          )
      ),
    );
  }

  Future<void> _refreshPIRA() async {
    setState(() {
      _regionsPIRA = pira.fetchData(http.Client());
    });
  }

  Widget piraWidget(List<pira.PIRA> regions) {

    // TODO: onTap show expanded tile with active, deaths, recovered, tests, population

    const _infoHeaderStyle = TextStyle(fontSize: 16.0);
    const _columnHeaderStyle = TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0);
    const _rowStyle = TextStyle(fontSize: 16.0);
    const _rowSmallerStyle = TextStyle(fontSize: 10.0);

    return ListView(physics: const AlwaysScrollableScrollPhysics(), children: <Widget>[
      Padding(
        padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 4.0),
        child: Text(
          pira.title,
          style: _infoHeaderStyle,
          textAlign: TextAlign.center,
        ),
      ),
      Padding(
          padding: EdgeInsets.only(bottom: 16.0),
          child: GestureDetector(
              child: Text(
                pira.url,
                style: _infoHeaderStyle.copyWith(color: Colors.blue),
                textAlign: TextAlign.center,
              ),
              onTap: () {
                _launchURL(pira.url);
              })),
      Padding(
        padding: EdgeInsets.only(bottom: 16.0),
        child: Text(
          DateTime.now().toIso8601String().substring(0, 16).replaceFirst("T", " "),
          style: _infoHeaderStyle,
          textAlign: TextAlign.center,
        ),
      ),
      DataTable(
        sortColumnIndex: 2,
        sortAscending: false,
        columns: <DataColumn>[
          DataColumn(
              numeric: false,
              label: Text(
                'Location',
                textAlign: TextAlign.left,
                style: _columnHeaderStyle,
              )),
          DataColumn(
              numeric: true,
              label: Text(
                'Risk',
                textAlign: TextAlign.center,
                style: _columnHeaderStyle,
              )),
          DataColumn(
              numeric: true,
              label: Text(
                'Rank',
                textAlign: TextAlign.center,
                style: _columnHeaderStyle,
              )),
        ],
        rows: regions.map<DataRow>((region) => DataRow(
            cells: <DataCell>[
              DataCell(
                Text.rich(
                  TextSpan(
                    text: region.location,
                    style: _rowStyle,
                    children: [
                      TextSpan(text: "\n", style: _rowSmallerStyle),
                      TextSpan(text: "T/P=${(region.tests/region.population).toStringAsFixed(3)}", style:
                      _rowSmallerStyle),
                      TextSpan(text: " A=${region.active.toString()}", style: _rowSmallerStyle),
                      TextSpan(text: " R=${region.recovered.toString()}", style: _rowSmallerStyle),
                    ],
                  ),
                  overflow: TextOverflow.visible,
                ),
              ),
              DataCell(Text(region.risk.toStringAsFixed(2), style: _rowStyle)),
              DataCell(Text(region.rank.toString(), style: _rowStyle)),
            ]))
            .toList(),
      )
    ]);
  }

  _launchURL(final url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw "Unable to launch $url";
    }
  }
}

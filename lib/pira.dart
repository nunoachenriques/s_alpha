/*
 * Copyright 2020 Nuno A. C. Henriques https://nunoachenriques.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

const title = "COVID-19 Population Indicator Analysis Assessment";
const url = "https://covid-19-pira.nunoachenriques.net";

Future<List<PIRA>> fetchData(http.Client client) async {
  final response = await client.get("$url/json");

  if (response.statusCode == HttpStatus.ok) { // HTTP status code 200 OK
    return compute(parseData, response.body);
  } else {
    throw HttpException("Fetch data FAILED with status $response.statusCode");
  }
}

List<PIRA> parseData(String responseBody) {
  final Map<String, dynamic> parsed = jsonDecode(responseBody);

  return parsed.entries.map<PIRA>((region) => PIRA.fromJson(region)).toList();
}

class PIRA {
  final String location;
  final int active;
  final int deaths;
  final int recovered;
  final int tests;
  final num population;
  final num risk;
  final num rank;

  PIRA({this.location, this.active, this.deaths, this.recovered, this.tests, this.population, this.risk, this.rank});

  factory PIRA.fromJson(MapEntry<String, dynamic> region) {
    final Map<String, dynamic> r = region.value;
    final num population = num.tryParse(r["population"].toString()) != null ? r["population"].round() : 0 / 0;
    final num risk = num.tryParse(r["risk"].toString()) != null ? r["risk"] : 0 / 0;
    final num rank = num.tryParse(r["rank"].toString()) != null ? r["rank"].round() : 0 / 0;
    return PIRA(
        location: region.key,
        active: r["active"],
        deaths: r["deaths"],
        recovered: r["recovered"],
        tests: r["tests"],
        population: population,
        risk: risk,
        rank: rank);
  }
}

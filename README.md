# s &alpha;

A testing facility to learn [Flutter](https://flutter.dev),
[Dart](https://dart.dev) and more...

The first feature is a data table with the
[COVID-19 PIRA](https://covid-19-pira.nunoachenriques.net) results.

![Android PIRA screenshot](android_screenshot.png)

## History

It all started in the context of the COVID-19 pandemic in 2020.

## License

Copyright 2020 Nuno A. C. Henriques https://nunoachenriques.net

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
